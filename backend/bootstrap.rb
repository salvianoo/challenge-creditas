class Payment
  attr_reader :authorization_number, :amount, :invoice, :order, :payment_method, :paid_at

  def initialize(attributes = {})
    @authorization_number, @amount = attributes.values_at(:authorization_number, :amount)
    @invoice, @order = attributes.values_at(:invoice, :order)
    @payment_method = attributes.values_at(:payment_method)
  end

  def pay(paid_at = Time.now)
    @amount = order.total_amount
    @authorization_number = Time.now.to_i
    @invoice = Invoice.new(billing_address: order.address, shipping_address: order.address, order: order)
    @paid_at = paid_at
    order.close(@paid_at)
  end

  def paid?
    !paid_at.nil?
  end
end

class Invoice
  attr_reader :billing_address, :shipping_address, :order

  def initialize(attributes = {})
    @billing_address = attributes.values_at(:billing_address)
    @shipping_address = attributes.values_at(:shipping_address)
    @order = attributes.values_at(:order)
  end
end

class Order
  attr_reader :customer, :items, :payment, :address, :closed_at

  def initialize(customer, overrides = {})
    @customer = customer
    @items = []
    @order_item_class = overrides.fetch(:item_class) { OrderItem }
    @address = overrides.fetch(:address) { Address.new(zipcode: '45678-979') }
  end

  def add_product(product)
    @items << @order_item_class.new(order: self, product: product)
  end

  def total_amount
    @items.map(&:total).inject(:+)
  end

  def close(closed_at = Time.now)
    @closed_at = closed_at
  end

  # remember: you can create new methods inside those classes to help you create a better design
end

class OrderItem
  attr_reader :order, :product

  def initialize(order:, product:)
    @order = order
    @product = product
  end

  def total
    10
  end
end

class Product
  # use type to distinguish each kind of product: physical, book, digital, membership, etc.
  attr_reader :name, :type

  def initialize(name:, type:)
    @name, @type = name, type
  end
end

class Address
  attr_reader :zipcode

  def initialize(zipcode:)
    @zipcode = zipcode
  end
end

class CreditCard
  def self.fetch_by_hashed(code)
    CreditCard.new
  end
end

class Customer
  # you can customize this class by yourself
end

class Membership
  # you can customize this class by yourself
end

class PhysicalItem
  def process
    shipping_label
  end

  def shipping_label; end
end

class BookItem
  def process
    shipping_label_without_taxes
  end
  def shipping_label_without_taxes; end
end

class DigitalItem
  def process
    send_email
  end

  def send_email; end
end

class MembershipItem
  def process
    activate_subscription
  end

  def activate_subscription; end
end

class ProcessOrder
  def initialize(order)
    @order = order
  end

  def process
    @order.items.each do |order_item|
      order_item.product.type.process
    end
  end
end

# Book Example (build new payments if you need to properly test it)
foolano = Customer.new
book  = Product.new(name: 'Awesome book', type: BookItem.new)
book2 = Product.new(name: 'Awesome book 2', type: BookItem.new)
ebook = Product.new(name: 'Awesome e-book', type: DigitalItem.new)
sneaker = Product.new(name: 'Awesome sneaker', type: PhysicalItem.new)
membership = Product.new(name: 'Awesome membership', type: MembershipItem.new)

order = Order.new(foolano)
order.add_product(book)
order.add_product(book2)
order.add_product(ebook)
order.add_product(sneaker)
order.add_product(membership)

payment = Payment.new(order: order, payment_method: CreditCard.fetch_by_hashed('43567890-987654367'))
payment.pay
p payment.paid? # < true
p payment.order.items.first.product.type

# now, how to deal with shipping rules then?
process_order = ProcessOrder.new(order)
process_order.process
