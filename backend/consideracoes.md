# Considerações

Na criação de cada `Product` é passado o atributo `name` e o `type` do produto. Que pode ser algum dos objetos abaixo:

- BookItem
- DigitalItem
- PhysicalItem
- MembershipItem

A lógica de entrega para cada tipo de produto fica encapsulada no metodo `process`.
Foi criado a classe `ProcessOrder` que recebe um objeto `Order`.
Para cada item do pedido será chamado o `process` do tipo de produto ao qual ele pertence.

Dessa maneira usando Polimorfismo é possivel substituir um código com vários condicionais.
